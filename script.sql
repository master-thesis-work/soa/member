create table member
(
    id           serial
        constraint member_pk
            primary key,
    first_name   varchar,
    last_name    varchar,
    phone_number varchar,
    email        varchar,
    is_active    boolean
);

alter table member
    owner to postgres;

create unique index member_id_uindex
    on member (id);

create table member_book
(
    id        serial
        constraint member_book_pk
            primary key,
    book_id   integer,
    member_id integer
        references member
);

alter table member_book
    owner to postgres;

create unique index member_book_id_uindex
    on member_book (id);

