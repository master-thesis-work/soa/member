package database

import (
	"context"
	"database/sql"
	"errors"
	"github.com/jmoiron/sqlx"
	"library/pkg/models"
)

type Memberer interface {
	CreateMember(member *models.Member) error
	UpdateMember(member *models.Member) error
	Members(page, limit int) ([]*models.Member, error)
	Member(id int) ([]*models.MemberBooks, error)
	DeleteMember(id int) error
	BorrowBook(bookID, memberID int) error
}

type Member struct {
	db *sqlx.DB
}

func NewMember(db *sqlx.DB) Memberer {
	return &Member{
		db: db,
	}
}

func (m *Member) CreateMember(member *models.Member) error {
	query := `INSERT INTO member(first_name, last_name, email, phone_number, is_active) VALUES (:first_name, :last_name, :email, :phone_number, true)`

	args := map[string]interface{}{
		"first_name":   member.FirstName,
		"last_name":    member.LastName,
		"email":        member.Email,
		"phone_number": member.PhoneNumber,
	}

	_, err := m.db.NamedExec(query, args)
	if err != nil {
		return err
	}

	return nil
}

func (m *Member) UpdateMember(member *models.Member) error {
	query := `UPDATE member SET first_name=:first_name, last_name=:last_name, email=:email, phone_number=:phone_number 
              WHERE id = :id`

	args := map[string]interface{}{
		"id":           member.ID,
		"first_name":   member.FirstName,
		"last_name":    member.LastName,
		"email":        member.Email,
		"phone_number": member.PhoneNumber,
	}

	_, err := m.db.NamedExec(query, args)
	if err != nil {
		return err
	}

	return nil
}

func (m *Member) Members(page, limit int) ([]*models.Member, error) {
	members := make([]*models.Member, 0)

	row, err := m.db.Queryx(`SELECT * FROM member ORDER BY ID DESC LIMIT $1 OFFSET $2`, limit, page)
	if err != nil {
		return nil, err
	}

	for row.Next() {
		var member models.Member

		err = row.StructScan(&member)
		if err != nil {
			return nil, err
		}

		members = append(members, &member)
	}

	if len(members) == 0 {
		return nil, sql.ErrNoRows
	}

	return members, nil
}

func (m *Member) Member(id int) ([]*models.MemberBooks, error) {
	memberBooks := make([]*models.MemberBooks, 0)

	query := `SELECT m.id,m.first_name, m.last_name, m.phone_number,  m.email, m.is_active, mb.book_id
			  FROM member m INNER JOIN member_book mb on m.id = mb.member_id WHERE mb.member_id = $1`

	row, err := m.db.Queryx(query, id)
	if err != nil {
		return nil, err
	}

	for row.Next() {
		memberBook := new(models.MemberBooks)

		memberBook.Member = new(models.Member)

		err = row.Scan(&memberBook.ID, &memberBook.FirstName, &memberBook.LastName, &memberBook.PhoneNumber, &memberBook.Email, &memberBook.IsActive, &memberBook.BookID)
		if err != nil {
			return nil, err
		}

		memberBooks = append(memberBooks, memberBook)
	}

	if len(memberBooks) == 0 {
		return nil, sql.ErrNoRows
	}

	return memberBooks, nil
}

func (m *Member) DeleteMember(id int) error {
	tx, err := m.db.BeginTxx(context.Background(), nil)
	if err != nil {
		return err
	}

	tx.MustExec("UPDATE member set is_active = false WHERE id=$1", id)

	err = tx.Commit()
	if err != nil {
		e := tx.Rollback()
		if err != nil {
			return e
		}

		return err
	}

	return nil
}

func (m *Member) BorrowBook(bookID, memberID int) error {
	var quantity int

	err := m.db.QueryRowx(`SELECT quantity FROM book where id= $1`, bookID).Scan(&quantity)
	if err != nil {
		return err
	}

	if quantity == 0 {
		return errors.New("book is not available")
	}

	tx, err := m.db.BeginTxx(context.Background(), nil)
	if err != nil {
		return err
	}

	tx.MustExec("UPDATE book set quantity = quantity - 1 WHERE id = $1", bookID)
	tx.MustExec("INSERT INTO member_book(book_id, member_id) VALUES($1,$2)", bookID, memberID)

	err = tx.Commit()
	if err != nil {
		e := tx.Rollback()
		if err != nil {
			return e
		}

		return err
	}

	return nil
}
