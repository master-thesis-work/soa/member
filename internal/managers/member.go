package managers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"library/internal/database"
	"library/pkg/models"
	"net/http"
)

type Memberer interface {
	CreateMember(member *models.Member) error
	UpdateMember(member *models.Member) error
	Members(page, limit int) ([]*models.Member, error)
	Member(id int) (*models.MemberBooks, error)
	DeleteMember(id int) error
	BorrowBook(bookID, memberID int) error
}

type Member struct {
	memberRepository database.Memberer
}

func NewMember(memberRepository database.Memberer) Memberer {
	return &Member{
		memberRepository: memberRepository,
	}
}

func (m Member) CreateMember(member *models.Member) error {
	return m.memberRepository.CreateMember(member)
}

func (m Member) UpdateMember(member *models.Member) error {
	return m.memberRepository.UpdateMember(member)
}

func (m Member) Members(page, limit int) ([]*models.Member, error) {
	return m.memberRepository.Members(page, limit)
}

func (m Member) Member(id int) (*models.MemberBooks, error) {
	member, err := m.memberRepository.Member(id)
	if err != nil {
		return nil, err
	}

	for i := range member {
		book, err := request(member[i].BookID)
		if err != nil {
			return nil, err
		}

		member[0].Books = append(member[0].Books, book)
	}

	return member[0], nil
}

func (m Member) DeleteMember(id int) error {
	return m.memberRepository.DeleteMember(id)
}

func (m Member) BorrowBook(bookID, memberID int) error {
	return m.memberRepository.BorrowBook(bookID, memberID)
}

func request(id int) (*models.Book, error) {
	url := fmt.Sprintf("http://localhost:8080/v1/gateway/book?id=%d", id)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, errors.New("error")
	}

	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	book := new(models.Book)

	err = json.Unmarshal(body, book)
	if err != nil {
		return nil, err
	}

	return book, nil
}
