package http

import (
	healthCheck "github.com/RaMin0/gin-health-check"
	"github.com/gin-gonic/gin"
	"library/internal/database"
	"library/internal/managers"
	"log"
)

type server struct {
	router *gin.Engine
	db     database.DataStore
}

func NewServer(db database.DataStore) *server {
	return &server{
		router: gin.New(),
		db:     db,
	}
}

func (srv *server) setupRouter() {
	srv.router.Use(gin.Recovery())
	srv.router.Use(healthCheck.Default())

	v1 := srv.router.Group("/v1")

	memberManager := managers.NewMember(srv.db.MemberRepository())
	NewMember(memberManager).Init(v1)
}

func (srv *server) Run() {
	srv.setupRouter()

	if err := srv.router.Run(":8082"); err != nil {
		log.Fatal(err)
	}
}
