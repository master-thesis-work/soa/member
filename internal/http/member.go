package http

import (
	"github.com/gin-gonic/gin"
	"library/internal/managers"
	"library/pkg/models"
	"net/http"
	"strconv"
)

type member struct {
	memberManager managers.Memberer
}

func NewMember(memberManager managers.Memberer) *member {
	return &member{
		memberManager: memberManager,
	}
}

func (m *member) Init(router *gin.RouterGroup) {
	route := router.Group("/member")

	route.GET("/members", m.members)
	route.POST("", m.createMember)
	route.GET("/:id", m.member)
	route.DELETE("/:id", m.deleteMember)
	route.PUT("/:id", m.updateMember)
	route.POST("/borrow", m.borrowBook)
}

func (m *member) members(ctx *gin.Context) {
	page := ctx.Query("page")
	limit := ctx.Query("limit")

	response, err := m.memberManager.Members(models.HandlePagination(limit, page))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})

		return
	}

	if len(response) == 0 {
		ctx.JSON(http.StatusNotFound, models.CustomResponse{
			Code:    http.StatusNotFound,
			Message: http.StatusText(http.StatusNotFound),
		})

		return
	}

	ctx.JSON(http.StatusOK, response)
}

func (m *member) createMember(ctx *gin.Context) {
	requestBody := new(models.Member)

	if err := ctx.BindJSON(requestBody); err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	err := m.memberManager.CreateMember(requestBody)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	ctx.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}

func (m *member) member(ctx *gin.Context) {
	idParam := ctx.Param("id")

	id, err := strconv.Atoi(idParam)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	response, err := m.memberManager.Member(id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})

		return
	}

	ctx.JSON(http.StatusOK, response)
}

func (m *member) deleteMember(ctx *gin.Context) {
	idParam := ctx.Param("id")

	id, err := strconv.Atoi(idParam)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	err = m.memberManager.DeleteMember(id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})

		return
	}

	ctx.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}

func (m *member) updateMember(ctx *gin.Context) {
	idParam := ctx.Param("id")

	id, err := strconv.Atoi(idParam)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	requestBody := new(models.Member)

	if err = ctx.BindJSON(requestBody); err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	requestBody.ID = id

	err = m.memberManager.UpdateMember(requestBody)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	ctx.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}

func (m *member) borrowBook(ctx *gin.Context) {
	requestBody := new(models.BorrowBookRequest)

	if err := ctx.BindJSON(requestBody); err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	err := m.memberManager.BorrowBook(requestBody.BookID, requestBody.MemberID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	ctx.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}
