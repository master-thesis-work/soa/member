package models

import "time"

type Book struct {
	ID          int       `json:"id" db:"id"`
	Quantity    int       `json:"quantity" db:"quantity"`
	Title       string    `json:"title" db:"title"`
	ISBN        string    `json:"isbn" db:"isbn"`
	ReleaseDate time.Time `json:"release_date" db:"release_date"`
}

type AddBookRequest struct {
	Book
	AuthorID int `json:"author_id"`
}
